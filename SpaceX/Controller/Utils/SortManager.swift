//
//  SortManager.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

// Sorting Options
enum SortingOption: String {
    case nameAsc, nameDesc, newest, oldest
}

// Sort Delegate Protocol
protocol SortDelegate: class {
    func sortDidFinish(_ sortManager: SortManager, sortedData: [Launch])
}

class SortManager {
    
    private let unsortedData: [Launch]
    weak var sortDelegate: SortDelegate?
    
    // Initialized with unsorted data
    init(unsortedData: [Launch]) {
        self.unsortedData = unsortedData
    }
    
    // Presents sort ActionSheet on specified VC
    func presentSortAlert(on vc: UIViewController) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        // ACTIONS
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let ascendingNameAction = UIAlertAction(title: "Name (asc)", style: .default) { _ in
            self.handleSort(option: .nameAsc)
        }
        let descendingNameAction = UIAlertAction(title: "Name (desc)", style: .default) { _ in
            self.handleSort(option: .nameDesc)
        }
        let ascendingDateAction = UIAlertAction(title: "Launch Date (newest)", style: .default) { _ in
            self.handleSort(option: .newest)
        }
        let descendingDateAction = UIAlertAction(title: "Launch Date (oldest)", style: .default) { _ in
            self.handleSort(option: .oldest)
        }

        actionSheet.addActions([ascendingNameAction, descendingNameAction, ascendingDateAction, descendingDateAction, cancelAction])
        vc.present(actionSheet, animated: true, completion: nil)
    }

    // Sort Handler - sorts unsorted data
    // Saves sort option in UserDefaults and calls sortDidFinish(_:sortedData:) delegate method
    func handleSort(option: SortingOption) {
        var sortedData: [Launch]
        
        switch option {
        case .nameAsc:
            sortedData = unsortedData.sorted(by: { ($0.missionName < $1.missionName) })
        case .nameDesc:
            sortedData = unsortedData.sorted(by: { ($0.missionName > $1.missionName) })
        case .oldest:
            sortedData = unsortedData.sorted(by: { ($0.launchDate < $1.launchDate) })
        case .newest:
            sortedData = unsortedData.sorted(by: { ($0.launchDate > $1.launchDate) })
        }
        
        PersistanceManager.saveSortingOption(option)
        sortDelegate?.sortDidFinish(self, sortedData: sortedData)
    }
    
}
