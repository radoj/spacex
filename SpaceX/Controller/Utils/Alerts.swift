//
//  Alerts.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

class Alerts {
    
    // Presents error alert with a message on specified view controller
    // We are using keyWindow's rootViewController as a default value if VC is not specified
    static func presentErrorAlert(on vc: UIViewController? = UIApplication.shared.keyWindow?.rootViewController, message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)

        alert.addAction(action)
        alert.view.layoutIfNeeded() // Prevents 'snapshotting a view...' log
        vc?.present(alert, animated: true, completion: nil)
    }
    
}
