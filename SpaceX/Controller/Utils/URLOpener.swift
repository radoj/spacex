//
//  URLOpener.swift
//  SpaceX
//
//  Created by Rado Jakubes on 17/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

class URLOpener {
    
    // Handle opening of URL, Youtube App is opening automatically in case of YT link
    // Presents error alert if URL is not valid
    func openWeb(url: String) {
        if let websiteURL = URL(string: url) {
            UIApplication.shared.open(websiteURL)
        } else {
            Alerts.presentErrorAlert(message: "Not Available")
        }
    }
    
}
