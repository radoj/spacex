//
//  LaunchTWDataResponder.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

// MARK: - TABLEVIEW DATA SOURCE
// also can't think of a better name than 'DataResponder'
class LaunchTWDataResponder: NSObject, UITableViewDataSource {
    
    private var data: [Launch] = []
    private var filteredData: [Launch] = []
    private var isSearching = false
    
    func updateData(_ data: [Launch]) {
        self.data = data
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getCurrentData().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.launch) as? LaunchCell else {
            return LaunchCell()
        }
        let launch = getCurrentData()[indexPath.row]
        
        cell.launch = launch
        cell.titleLabel.text = launch.missionName
        return cell
    }
    
}

// MARK: - SEARCHBAR DELEGATE
extension LaunchTWDataResponder: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // dismiss keyboard
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            
            isSearching = false
            
        } else {
            
            isSearching = true
            // filter initial data to match searchtext in case-insensitive matter
            filteredData = data.filter({ $0.missionName.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil })
            
        }
        // post notification to reload tableview data
        NotificationCenter.default.post(name: .updateLaunchTW, object: nil)
    }
    
}

// MARK: - HELPERS
extension LaunchTWDataResponder {
    
    func resetSearching(of searchBar: UISearchBar) {
        searchBar.text = nil
        isSearching = false
        filteredData = []
    }
    
    func getCurrentData() -> [Launch] {
        return isSearching ? filteredData : data
    }
    
    func getInitialData() -> [Launch] {
        return data
    }
    
}
