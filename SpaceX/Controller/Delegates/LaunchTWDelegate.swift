//
//  LaunchTableViewDelegate.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

// SEGUE DELEGATE PROTOCOL
protocol DetailsSegueDelegate: class {
    func presentDetails(of launch: Launch)
}

class LaunchTWDelegate: NSObject, UITableViewDelegate {
    
    weak var detailsSegueDelegate: DetailsSegueDelegate?
    
    // Calls presentDetails(of:) delegate function to segue to another VC
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? LaunchCell else {
            print("TW DELEGATE: ERROR PARSING TO LAUNCHCELL")
            return
        }
        detailsSegueDelegate?.presentDetails(of: cell.launch)
    }
    
}
