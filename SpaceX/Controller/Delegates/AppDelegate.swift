//
//  AppDelegate.swift
//  SpaceX
//
//  Created by Rado Jakubes on 15/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}
