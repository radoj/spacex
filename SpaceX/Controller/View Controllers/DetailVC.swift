//
//  DetailVC.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

class DetailVC: UIViewController {
    
    @IBOutlet weak var missionNameLabel: UILabel!
    @IBOutlet weak var launchSiteLabel: UILabel!
    @IBOutlet weak var launchDateLabel: UILabel!
    @IBOutlet weak var isSuccessfulLabel: UILabel!
    @IBOutlet weak var detailsTextView: UITextView!
    @IBOutlet weak var rocketNameLabel: UILabel!
    @IBOutlet weak var rocketTypeLabel: UILabel!
    
    private let urlOpener = URLOpener()
    var launch: Launch! // can't be nil 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    // Setup of texts
    private func setupViews() {
        title = String(launch.flightNumber)
        missionNameLabel.text = launch.missionName
        launchSiteLabel.text = launch.launchSiteName
        isSuccessfulLabel.text = launch.isUpcoming ? "Upcoming" : launch.isSuccessful ? "Successful" : "Failed"
        detailsTextView.text = launch.details.isEmpty ? "No details provided" : launch.details
        rocketNameLabel.text = launch.rocket.rocketName
        rocketTypeLabel.text = launch.rocket.rocketType
        launchDateLabel.text = "\(launch.launchDate)" // Use DateFormatter for different format
    }
    
}

// MARK: IB ACTIONS
extension DetailVC {
    
    // Opens Wikipedia in Web Browser
    @IBAction func wikipediaTapped(_ sender: Any) {
        urlOpener.openWeb(url: launch.links.wikipediaURL)
    }
    
    // Opens Youtube in App / Web Browser
    @IBAction func videoTapped(_ sender: Any) {
        urlOpener.openWeb(url: launch.links.videoURL)
    }
    
}
