//
//  ListVC.swift
//  SpaceX
//
//  Created by Rado Jakubes on 15/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

class ListVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    private let launchFetcher = LaunchFetcher()
    private let tableViewDataResponder = LaunchTWDataResponder()
    private let tableViewDelegate = LaunchTWDelegate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    private func initialSetup() {
        tableView.tableFooterView = UIView()
        tableView.dataSource = tableViewDataResponder
        searchBar.delegate = tableViewDataResponder
        tableView.delegate = tableViewDelegate
        tableViewDelegate.detailsSegueDelegate = self
        
        addNotificationObservers()
        fetchData()
    }
    
    private func addNotificationObservers() {
        // I'd use delegate, since we don't need to use to-many communication, but I just want to demonstrate this approach
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTableViewData), name: .updateLaunchTW, object: nil)
    }
    
    // TW Data Fetcher
    private func fetchData() {
        launchFetcher.retrieveLaunches { [weak self] (launches, error) in

            if let strongSelf = self {
                DispatchQueue.main.async {
                    // Indicator is animating on load and hides when animation stop
                    strongSelf.loadingIndicator.stopAnimating() // view!
                    if let launches = launches {
                        
                        let savedSortingOption = PersistanceManager.getSavedSortingOption()
                        // If savedSortingOption is nil, than user haven't ever sorted TW = just update TW
                        // We can register default sorting option in UserDefaults if we want to (in application(_:didFinishLaunchingWithOptions:))
                        if let savedSortingOption = savedSortingOption {
                            let sortManager = SortManager(unsortedData: launches)
                            sortManager.sortDelegate = self
                            // handling sort also updates TW via sortDidFinish(_: sortedData:) delegate method
                            sortManager.handleSort(option: savedSortingOption)
                        } else {
                            strongSelf.updateTableViewData(with: launches)
                        }

                    } else {
                        
                        // Handles API Error
                        Alerts.presentErrorAlert(on: strongSelf, message: error?.localizedDescription ?? "No SpaceX data found, please try again later")
                        
                    }
                    
                }
            }
            
        }
    }
    
}

// MARK: - IB ACTIONS
extension ListVC {
    
    // Presents Sort ActionSheet
    @IBAction func sortTapped(_ sender: Any) {
        view.endEditing(true)
        let sortManager = SortManager(unsortedData: tableViewDataResponder.getInitialData())
        sortManager.sortDelegate = self
        sortManager.presentSortAlert(on: self)
    }
    
}

// MARK: - TABLEVIEW HELPERS
extension ListVC {
    
    private func updateTableViewData(with launches: [Launch]) {
        tableViewDataResponder.updateData(launches)
        reloadTableViewData()
    }
    
    @objc private func reloadTableViewData() {
        tableView.reloadData()
    }
    
}

// MARK: - DETAILS SEGUE DELEGATE
extension ListVC: DetailsSegueDelegate {
    
    // Presents Details VC
    func presentDetails(of launch: Launch) {
        view.endEditing(true)
        navigationController?.pushVC(.detail, from: .main, additionalSetup: { (vc) in
            let vc = vc as! DetailVC
            vc.launch = launch
        })
    }
    
}

// MARK: - SORT DELEGATE
extension ListVC: SortDelegate {
    
    // Resets searching and updates TW with sorted data
    func sortDidFinish(_ sortManager: SortManager, sortedData: [Launch]) {
        tableViewDataResponder.resetSearching(of: searchBar)
        updateTableViewData(with: sortedData)
    }
    
}
