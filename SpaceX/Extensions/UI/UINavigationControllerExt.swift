//
//  UINavigationControllerExt.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    func pushVC(_ vc: VCIdentifiers, from storyboard: StoryboardIdentifiers, additionalSetup: ((UIViewController) -> Void)?) {
        let storyboard = UIStoryboard.init(name: storyboard.rawValue, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: vc.rawValue)
        
        additionalSetup?(controller)
        pushViewController(controller, animated: true)
    }
    
}
