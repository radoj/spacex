//
//  LaunchFetcher.swift
//  SpaceX
//
//  Created by Rado Jakubes on 17/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import Alamofire
import SwiftyJSON

class LaunchFetcher {
    
    private let launchParser = LaunchParser()
    typealias FetcherCompletionHandler = ([Launch]?, Error?) -> ()
    
    // Makes Alamofire request to SpaceX API
    // and calls FetcherCompletionHandler with launches (parsed via LaunchParser) or error
    func retrieveLaunches(completion: @escaping FetcherCompletionHandler) {
        Alamofire.request(APIIdentifiers.launchesURL).validate().responseJSON { [weak self] response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                self?.launchParser.parseJSON(json, completion: { (launches) in
                    completion(launches, nil)
                })
            case .failure(let error):
                print("LAUNCHFETCHER: ", error.localizedDescription)
                completion(nil, error)
            }
            
        }
    }
    
}
