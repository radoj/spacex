//
//  PersistanceManager.swift
//  SpaceX
//
//  Created by Rado Jakubes on 17/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import Foundation

class PersistanceManager {
    
    // Saves sort option's raw value to User Defaults
    static func saveSortingOption(_ option: SortingOption) {
        UserDefaults.standard.set(option.rawValue, forKey: UserDefaultsIdentifiers.sortingOption)
    }

    // Get's sorting option from User Defaults
    static func getSavedSortingOption() -> SortingOption? {
        let sortingOptionStr = UserDefaults.standard.string(forKey: UserDefaultsIdentifiers.sortingOption)
        return sortingOptionStr == nil ? nil : SortingOption(rawValue: sortingOptionStr!)
    }

}
