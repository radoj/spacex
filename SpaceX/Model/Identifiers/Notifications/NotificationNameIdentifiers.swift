//
//  NotificationNameIdentifiers.swift
//  SpaceX
//
//  Created by Rado Jakubes on 17/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let updateLaunchTW = Notification.Name(rawValue: "UpdateLaunchTW")
    
}
