//
//  StoryboardIdentifiers.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import Foundation

enum StoryboardIdentifiers: String {
    
    case main = "Main"
    
}
