//
//  APIIdentifiers.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

enum APIIdentifiers {
    
    static let launchesURL = "https://api.spacexdata.com/v3/launches"
    static let flightNumber = "flight_number"
    static let launchDateUnix = "launch_date_unix"
    static let missionName = "mission_name"
    static let rocket = "rocket"
    static let rocketName = "rocket_name"
    static let rocketType = "rocket_type"
    static let launchSite = "launch_site"
    static let launchSiteName = "site_name_long"
    static let launchSuccess = "launch_success"
    static let upcoming = "upcoming"
    static let details = "details"
    static let links = "links"
    static let wikipedia = "wikipedia"
    static let video = "video_link"
    
}
