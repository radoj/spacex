//
//  CellIdentifiers.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

enum CellIdentifiers {
    
    static let launch = "LaunchCell"
    
}
