//
//  Launch.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import Foundation

// We're not including image URL, since many of them doesn't have any
struct Launch {
    
    let missionName: String
    let launchSiteName: String
    let flightNumber: Int
    let launchDate: Date
    let details: String
    let isSuccessful: Bool
    let isUpcoming: Bool
    let rocket: LaunchRocket
    let links: LaunchLinks
    
}
