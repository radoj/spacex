//
//  LaunchParser.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import SwiftyJSON

class LaunchParser {
    
    typealias ParserCompletionHandler = ([Launch]?) -> ()

    // Parsing function for JSON that is received via SwiftyJSON from Alamofire request [LaunchFetcher]
    func parseJSON(_ json: JSON, completion: @escaping ParserCompletionHandler) {
        if let launchesArray = json.array {
            
            DispatchQueue.global(qos: .background).async {
                var launches: [Launch] = []
                for launch in launchesArray {
                    let missionName = launch[APIIdentifiers.missionName].stringValue
                    let launchSiteName = launch[APIIdentifiers.launchSite][APIIdentifiers.launchSiteName].stringValue
                    let launchDate = Date(timeIntervalSince1970: launch[APIIdentifiers.launchDateUnix].doubleValue)
                    let flightNumber = launch[APIIdentifiers.flightNumber].intValue
                    let details = launch[APIIdentifiers.details].stringValue
                    let isSuccessful = launch[APIIdentifiers.launchSuccess].boolValue
                    let isUpcoming = launch[APIIdentifiers.upcoming].boolValue
                    let rocketName = launch[APIIdentifiers.rocket][APIIdentifiers.rocketName].stringValue
                    let rocketType = launch[APIIdentifiers.rocket][APIIdentifiers.rocketType].stringValue
                    let wikiURL = launch[APIIdentifiers.links][APIIdentifiers.wikipedia].stringValue
                    let videoURL = launch[APIIdentifiers.links][APIIdentifiers.video].stringValue
                    
                    let launchURLs = LaunchLinks.init(wikipediaURL: wikiURL, videoURL: videoURL)
                    let launchRocket = LaunchRocket.init(rocketName: rocketName, rocketType: rocketType)
                    let launch = Launch.init(missionName: missionName, launchSiteName: launchSiteName, flightNumber: flightNumber, launchDate: launchDate, details: details, isSuccessful: isSuccessful, isUpcoming: isUpcoming, rocket: launchRocket, links: launchURLs)
                    launches.append(launch)
                }
                
                completion(launches)
            }
            
        } else {
            
            print("LAUNCHPARSER: ERROR RETRIEVEING LAUNCHES ARRAY")
            completion(nil)

        }
    }
    
}
