//
//  LaunchCell.swift
//  SpaceX
//
//  Created by Rado Jakubes on 16/10/2018.
//  Copyright © 2018 Rado Jakubes. All rights reserved.
//

import UIKit

class LaunchCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    var launch: Launch! // Storing this property to identify cell's launch in tableView(_:didSelectRowAt:) [LaunchTWDelegate]
    
}
